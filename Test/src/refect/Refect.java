package refect;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import vo.Person;



public class Refect {
public Refect(){}
	
	public static Person full(HttpServletRequest request){
		Person o  = null;
		Class clazz;
		try {
			clazz = Class.forName(request.getParameter("sign"));//创建某个类的class对象
			o = (Person) clazz.newInstance();//调用默认构造器创建对象
			Field[] f_ar = clazz.getDeclaredFields();//获取对象的属性集合
			for (Field f : f_ar) {
				f.setAccessible(true);//
				f.set(o, request.getParameter(f.getName()));//给某个属性赋值
				f.setAccessible(false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return o;}
}
