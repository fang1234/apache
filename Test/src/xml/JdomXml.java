package xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.registry.infomodel.User;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import vo.Person;




public class JdomXml {
	

	public static Map<String, Person> jiexiXml(String filename) throws FileNotFoundException, JDOMException, IOException {
		Map<String, Person> map = new HashMap<String, Person>();
		
		//获取sax解析方式
		SAXBuilder saxBuilder = new SAXBuilder();
		//获取指定的文件流  并创建文档对象
		Document document = saxBuilder.build(new FileInputStream(filename));
		
		//通过文档对象获取根节点
		Element root = document.getRootElement();
		
		List<Element> users =root.getChildren();
		for (Element element : users) {
			System.out.println(element.getName());
			Person person  = new Person();
			
			person.setName(element.getChild("name").getValue());
			person.setName(element.getChild("password").getValue());
			person.setName(element.getChild("age").getValue());
			person.setName(element.getChild("sex").getValue());
			map.put(person.getName(), person);
		}

	return map;

	
	}
	
}
