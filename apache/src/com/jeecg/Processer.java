package com.jeecg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processer extends Thread {

    private Socket socket;
    private InputStream in;
    private PrintStream out;
    public final static String WEB_ROOT = "E:\\Workspace\\struts\\WebContent\\view\\";

    public Processer(Socket socket) {
        this.socket = socket;
        try {
            in = socket.getInputStream();
            out = new PrintStream(socket.getOutputStream());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void run() {
        String fileName = parse(in);
        sendFile(fileName);
    }

    public String parse(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String fileName = "";
        try {
            String httpMessage = br.readLine();
            String[] content = httpMessage.split(" ");
            if (content.length != 3) {
                sendErrorMessage(400, "Client query error!");
                return null;
            }
            fileName = content[1];
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return fileName;
    }

    public void sendErrorMessage(int errorCode, String errorMessage) {
        out.println("HTTP/1.0 " + errorCode + " " + errorMessage);
        out.println("content-type: text/html");
        out.println();
        out.println("<html>");
        out.println("<title>Error Message</title>");
        out.println("<body>");
        out.println("<h1>ErrorCode:"+errorCode+",ErrorMessage:"+errorMessage+"</h1>");
        out.println("</body>");
        out.println("</html>");
        out.flush();
        out.close();
        try {
            in.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void sendFile(String fileName) {
        File file = new File(Processer.WEB_ROOT + fileName);
        if (!file.exists()) {
            sendErrorMessage(404, "File Not Found!");
            return;
        }
        try {
            InputStream inputStream = new FileInputStream(file);
            byte[] content = new byte[(int) file.length()];
            inputStream.read(content);
            out.println("HTTP/1.0 200 queryFile");
            out.println("content-lenght: " + content.length);
            out.println();
            out.write(content);
            out.flush();
            out.close();
            in.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
